import Image from "@mui/icons-material/Image";
import ToggleButton from "@mui/material/ToggleButton";
import React, { FC, useCallback, useState } from "react";
import { EditorImageElement, ToolbarImageButtonProps } from "@uxf/wysiwyg";
import { ImageUploadDialog } from "./ImageUploadDialog";

export const ImageElementButton: FC<ToolbarImageButtonProps> = ({ editor, imageNamespace, imageUploadHandler }) => {
    const [dialogOpen, setDialogOpen] = useState<boolean>(false);
    const [activeImage, setActiveImage] = useState<EditorImageElement | null>(null);

    const closeDialog = useCallback(() => setDialogOpen(false), []);
    return (
        <>
            <ToggleButton
                key="image-button"
                size="small"
                value="image-button"
                title="Obrázek"
                aria-label="image-button"
                selected={editor.isImageActive(editor)}
                onClick={e => {
                    e.preventDefault();
                    e.persist();
                    setActiveImage(editor.getActiveImage(editor));
                    setDialogOpen(true);
                }}
            >
                <Image />
            </ToggleButton>
            <ImageUploadDialog
                open={dialogOpen}
                onClose={closeDialog}
                imageUploadHandler={imageUploadHandler}
                imageNamespace={imageNamespace}
                editor={editor}
                editedImage={activeImage}
            />
        </>
    );
};
