import { createTheme, ThemeProvider, StyledEngineProvider } from "@mui/material/styles";
import React from "react";
import { render } from "react-dom";
import { App } from "./App";

render(
    <StyledEngineProvider injectFirst>
        <ThemeProvider theme={createTheme()}>
            <App />
        </ThemeProvider>
    </StyledEngineProvider>,
    document.getElementById("app"),
);
