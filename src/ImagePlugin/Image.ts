import { styled } from "@mui/material/styles";

export const Image = styled("img")<{ selected: boolean; focused: boolean }>(({ theme, selected, focused }) => ({
    display: "block",
    maxWidth: "100%",
    boxShadow: selected && focused ? `0 0 0 3px ${theme.palette.action.active}` : "none",
}));
