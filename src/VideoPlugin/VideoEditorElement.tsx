import React from "react";
import ReactPlayer from "react-player/lazy";
import { RenderElementProps } from "slate-react";

export const VideoEditorElement = ({ attributes, element }: Omit<RenderElementProps, "children">) => {
    return (
        <div {...attributes}>
            <ReactPlayer
                url={typeof element.url === "string" ? element.url : ""}
                controls={true}
                width="100%"
                height="100%"
            />
        </div>
    );
};
