import { Box } from "@mui/material";
import { ToolbarComponent } from "@uxf/wysiwyg";
import React from "react";
import { useSlate } from "slate-react";

export const Toolbar: ToolbarComponent = ({ plugins }) => {
    const editor = useSlate();
    return (
        <Box
            sx={{
                display: "flex",
                flexWrap: "wrap",
                alignItems: "center",
                width: "auto",
                position: "sticky",
                top: 0,
                backgroundColor: "#fff",
                zIndex: 3,
                border: theme => `1px solid ${theme.palette.divider}`,
                borderRadius: theme => theme.spacing(0.5, 0.5, 0, 0),
                ".MuiToggleButton-root": {
                    border: 0,
                    width: 38,
                    height: 38,
                },
            }}
        >
            {plugins.map((Plugin, index) => {
                if ("buttonDivider" in Plugin) {
                    return <Plugin.buttonDivider key={`divider-${index}`} />;
                }

                if (typeof Plugin.toolbarButton === "undefined") {
                    return null;
                }

                if ("imageUploadHandler" in Plugin && "imageNamespace" in Plugin) {
                    return (
                        <Plugin.toolbarButton
                            key={Plugin.type + index}
                            editor={editor}
                            imageNamespace={Plugin.imageNamespace}
                            imageUploadHandler={Plugin.imageUploadHandler}
                        />
                    );
                }

                return <Plugin.toolbarButton key={Plugin.type + index} editor={editor} />;
            })}
        </Box>
    );
};
