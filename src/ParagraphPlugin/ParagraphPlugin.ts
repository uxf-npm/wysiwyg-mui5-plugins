import { EditorElementPlugin } from "@uxf/wysiwyg";
import { ParagraphEditorElement } from "./ParagraphEditorElement";

export const ParagraphPlugin: EditorElementPlugin = {
    editorElementRenderer: ParagraphEditorElement,
    type: "paragraph",
};
