import MuiDivider from "@mui/material/Divider";
import { ToolbarButtonDivider } from "@uxf/wysiwyg";
import React, { FC } from "react";

const DividerComponent: FC = () => {
    return (
        <MuiDivider
            sx={{
                alignSelf: "stretch",
                height: "auto",
                margin: theme => theme.spacing(1, 0.5),
            }}
            orientation="vertical"
            flexItem
        />
    );
};

export const Divider: ToolbarButtonDivider = {
    buttonDivider: DividerComponent,
};
