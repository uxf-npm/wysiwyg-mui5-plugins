import { EditorElementPlugin } from "@uxf/wysiwyg";
import { ListItemEditorElement } from "./ListItemEditorElement";

export const ListItemPlugin: EditorElementPlugin = {
    editorElementRenderer: ListItemEditorElement,
    type: "list-item",
};
