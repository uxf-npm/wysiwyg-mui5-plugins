import { EditorLeafPlugin } from "@uxf/wysiwyg";
import { BoldButton } from "./BoldButton";
import { BoldEditorRenderer } from "./BoldEditorRenderer";

export const BoldPlugin: EditorLeafPlugin = {
    toolbarButton: BoldButton,
    editorLeafRenderer: BoldEditorRenderer,
    type: "bold",
};
