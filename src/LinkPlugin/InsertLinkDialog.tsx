import { Checkbox } from "@mui/material";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import FormControlLabel from "@mui/material/FormControlLabel";
import TextField from "@mui/material/TextField";
import { EditorLinkElement, UXFEditor } from "@uxf/wysiwyg";
import React, { FC, Reducer, useCallback, useEffect, useReducer } from "react";
import { Range } from "slate";

export interface InsertLinkDialogProps {
    open: boolean;
    onClose: () => void;
    editor: UXFEditor;
    editedLink: EditorLinkElement | null;
}

const PROTOCOL = "https://";
type Target = "_self" | "_blank";

interface State {
    url: string;
    text: string;
    target?: Target;
}

const initialState: State = {
    url: PROTOCOL,
    text: "",
    target: undefined,
};

type Action =
    | { type: "reset" }
    | { type: "setUrl"; url: string }
    | { type: "setText"; text: string }
    | { type: "setTarget"; target?: Target }
    | { type: "setEditedLink"; target?: Target; url: string };

const reducer: Reducer<State, Action> = (prevState, action) => {
    switch (action.type) {
        case "reset":
            return initialState;
        case "setUrl":
            return { ...prevState, url: action.url };
        case "setText":
            return { ...prevState, text: action.text };
        case "setTarget":
            return {
                ...prevState,
                target: action.target,
            };
        case "setEditedLink":
            return {
                ...prevState,
                target: action.target,
                url: action.url,
            };
        default:
            throw Error();
    }
};

export const InsertLinkDialog: FC<InsertLinkDialogProps> = ({ open, onClose, editor, editedLink }) => {
    const [state, dispatch] = useReducer(reducer, { ...initialState });

    const isCollapsed = editor.selection && Range.isCollapsed(editor.selection);

    useEffect(() => {
        if (editedLink && !isCollapsed) {
            dispatch({
                type: "setEditedLink",
                url: editedLink.url ? editedLink.url : PROTOCOL,
                target: editedLink.target as Target,
            });
        }
    }, [editedLink, isCollapsed]);

    const resetState = useCallback(() => {
        dispatch({ type: "reset" });
    }, []);

    // OK button
    const okButtonHandler = () => {
        editor.insertLink(editor, state.url, state.text, state.target);
        resetState();
        onClose();
    };

    // Cancel button
    const closeHandler = useCallback(() => {
        resetState();
        onClose();
    }, [onClose, resetState]);

    // Remove button
    const removeHandler = useCallback(() => {
        editor.removeLink(editor);
        resetState();
        onClose();
    }, [editor, onClose, resetState]);

    return (
        <Dialog open={open} onClose={closeHandler} aria-labelledby="insert-link-dialog" fullWidth>
            <DialogTitle id="insert-link-dialog-title">
                {editedLink && !isCollapsed ? "Upravit odkaz" : "Vložit odkaz"}
            </DialogTitle>
            <DialogContent>
                {isCollapsed && (
                    <TextField
                        /* eslint-disable-next-line jsx-a11y/no-autofocus */
                        autoFocus
                        margin="dense"
                        size="small"
                        id="text"
                        label="Text odkazu"
                        type="text"
                        fullWidth
                        value={state.text}
                        onChange={e => dispatch({ type: "setText", text: e.target.value })}
                    />
                )}
                <TextField
                    margin="dense"
                    size="small"
                    /* eslint-disable-next-line jsx-a11y/no-autofocus */
                    autoFocus={!isCollapsed}
                    id="url"
                    label="URL odkazu"
                    type="text"
                    fullWidth
                    value={state.url}
                    required
                    onChange={e => dispatch({ type: "setUrl", url: e.target.value })}
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={state.target === "_blank"}
                            onChange={e =>
                                dispatch({ type: "setTarget", target: e.target.checked ? "_blank" : undefined })
                            }
                            name="newTab"
                        />
                    }
                    label="Otevřít v nové záložce"
                />
            </DialogContent>

            <DialogActions>
                {editedLink && !isCollapsed && (
                    <Button onClick={removeHandler} color="secondary">
                        Odebrat odkaz
                    </Button>
                )}
                <div style={{ flex: "1 0 0" }} />
                <Button onClick={closeHandler}>Zrušit</Button>
                <Button onClick={okButtonHandler} color="primary" variant="contained">
                    {editedLink && !isCollapsed ? "Upravit odkaz" : "Vložit odkaz"}
                </Button>
            </DialogActions>
        </Dialog>
    );
};
