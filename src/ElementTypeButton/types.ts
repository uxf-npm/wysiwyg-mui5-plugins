import { ReactElement } from "react";
import { SupportedBlockElementTypes, ToolbarButtonProps } from "@uxf/wysiwyg";

export interface ElementTypeButtonProps<SUPPORTED_TYPES = string> extends ToolbarButtonProps {
    name: SupportedBlockElementTypes<SUPPORTED_TYPES>;
    title: string;
    label: ReactElement;
}
