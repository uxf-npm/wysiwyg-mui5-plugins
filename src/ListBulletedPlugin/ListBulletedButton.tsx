import FormatListBulletedIcon from "@mui/icons-material/FormatListBulleted";
import React, { FC } from "react";
import { ToolbarButtonProps } from "@uxf/wysiwyg";
import { ElementTypeButton } from "../ElementTypeButton";

export const ListBulletedButton: FC<ToolbarButtonProps> = ({ editor }) => (
    <ElementTypeButton
        editor={editor}
        label={<FormatListBulletedIcon />}
        name="bulleted-list"
        title="Odrážkový seznam"
    />
);
