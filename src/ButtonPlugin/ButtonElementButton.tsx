import ToggleButton from "@mui/material/ToggleButton";
import { EditorButtonElement, ToolbarButtonProps } from "@uxf/wysiwyg";
import React, { FC, useCallback, useState } from "react";
import { InsertButtonDialog } from "./InsertButtonDialog";

export const ButtonElementButton: FC<ToolbarButtonProps> = ({ editor }) => {
    const [dialogOpen, setDialogOpen] = useState<boolean>(false);
    const [activeButton, setActiveButton] = useState<EditorButtonElement | null>(null);

    const closeDialog = useCallback(() => setDialogOpen(false), []);

    return (
        <>
            <ToggleButton
                key="insert-button-button"
                size="small"
                value="button"
                aria-label="button"
                title="Tlačítko"
                selected={editor.isButtonActive(editor)}
                onMouseDown={event => {
                    event.preventDefault();
                    event.persist();
                    setActiveButton(editor.getActiveButton(editor));
                    setDialogOpen(true);
                }}
            >
                BTN
            </ToggleButton>
            <InsertButtonDialog open={dialogOpen} onClose={closeDialog} editor={editor} editedButton={activeButton} />
        </>
    );
};
