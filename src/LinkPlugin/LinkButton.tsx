import Link from "@mui/icons-material/Link";
import ToggleButton from "@mui/material/ToggleButton";
import React, { FC, useCallback, useState } from "react";
import { EditorLinkElement, ToolbarButtonProps } from "@uxf/wysiwyg";
import { InsertLinkDialog } from "./InsertLinkDialog";

export const LinkButton: FC<ToolbarButtonProps> = ({ editor }) => {
    const [dialogOpen, setDialogOpen] = useState<boolean>(false);
    const [activeLink, setActiveLink] = useState<EditorLinkElement | null>(null);

    const closeDialog = useCallback(() => setDialogOpen(false), []);
    return (
        <>
            <ToggleButton
                key="link-button"
                size="small"
                value="link"
                aria-label="link"
                selected={editor.isLinkActive(editor)}
                title="Odkaz"
                onMouseDown={event => {
                    event.preventDefault();
                    event.persist();
                    setActiveLink(editor.getActiveLink(editor));
                    setDialogOpen(true);
                }}
            >
                <Link />
            </ToggleButton>
            <InsertLinkDialog open={dialogOpen} onClose={closeDialog} editor={editor} editedLink={activeLink} />
        </>
    );
};
