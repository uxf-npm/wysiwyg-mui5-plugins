import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import TextField from "@mui/material/TextField";
import { EditorButtonElement, UXFEditor } from "@uxf/wysiwyg";
import React, { FC, Reducer, useCallback, useEffect, useReducer } from "react";
import { Transforms } from "slate";

export interface InsertLinkDialogProps {
    open: boolean;
    onClose: () => void;
    editor: UXFEditor;
    editedButton: EditorButtonElement | null;
}

interface State {
    url: string;
    text: string;
}

const initialState: State = {
    url: "",
    text: "",
};

type Action =
    | { type: "reset" }
    | { type: "setUrl"; url: string }
    | { type: "setText"; text: string }
    | { type: "setEditedButton"; text: string; url: string };

const reducer: Reducer<State, Action> = (prevState, action) => {
    switch (action.type) {
        case "reset":
            return initialState;
        case "setUrl":
            return { ...prevState, url: action.url };
        case "setText":
            return { ...prevState, text: action.text };
        case "setEditedButton":
            return {
                ...prevState,
                text: action.text,
                url: action.url,
            };
        default:
            throw Error();
    }
};

export const InsertButtonDialog: FC<InsertLinkDialogProps> = ({ open, onClose, editor, editedButton }) => {
    const [state, dispatch] = useReducer(reducer, { ...initialState });

    useEffect(() => {
        if (editedButton) {
            dispatch({
                type: "setEditedButton",
                url: editedButton.buttonUrl ? editedButton.buttonUrl : "",
                text: editedButton.buttonText ? editedButton.buttonText : "",
            });
        }
    }, [editedButton]);

    const resetState = useCallback(() => {
        dispatch({ type: "reset" });
    }, []);

    // OK button
    const okButtonHandler = () => {
        if (state.url && state.text && !editedButton) {
            editor.insertButton(editor, state.text, state.url, editor.selection);
        }

        if (editedButton) {
            Transforms.setNodes(
                editor,
                {
                    buttonUrl: state.url,
                    buttonText: state.text,
                },
                { at: editor.selection ?? undefined },
            );
        }

        resetState();
        onClose();
    };

    // Cancel button
    const closeHandler = useCallback(() => {
        resetState();
        onClose();
    }, [onClose, resetState]);

    return (
        <Dialog open={open} onClose={closeHandler} aria-labelledby="insert-btn-dialog" fullWidth>
            <DialogTitle>{editedButton ? "Upravit tlačítko" : "Vložit tlačítko"}</DialogTitle>
            <DialogContent>
                <TextField
                    /* eslint-disable-next-line jsx-a11y/no-autofocus */
                    autoFocus
                    margin="dense"
                    size="small"
                    label="Text tlačítka"
                    type="text"
                    fullWidth
                    value={state.text}
                    onChange={e => dispatch({ type: "setText", text: e.target.value })}
                />
                <TextField
                    margin="dense"
                    size="small"
                    label="URL tlačítka"
                    type="text"
                    fullWidth
                    value={state.url}
                    required
                    onChange={e => dispatch({ type: "setUrl", url: e.target.value })}
                />
            </DialogContent>

            <DialogActions>
                <div style={{ flex: "1 0 0" }} />
                <Button onClick={closeHandler}>Zrušit</Button>
                <Button onClick={okButtonHandler} color="primary" variant="contained">
                    {editedButton ? "Upravit tlačítko" : "Vložit tlačítko"}
                </Button>
            </DialogActions>
        </Dialog>
    );
};
