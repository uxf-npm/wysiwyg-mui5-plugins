import { Box, Button } from "@mui/material";
import React, { FC } from "react";
import { EditorElementRendererProps } from "@uxf/wysiwyg";

export const ButtonEditorElement: FC<EditorElementRendererProps> = ({ attributes, children, element }) => (
    <Box {...attributes} display="flex" justifyContent="center" mb={2}>
        <Button href={element.buttonUrl} variant="outlined">
            {element.buttonText}
        </Button>
        {children}
    </Box>
);
