import { EditorElementPlugin } from "@uxf/wysiwyg";
import { BlockQuoteButton } from "./BlockQuoteButton";
import { BlockQuoteEditorElement } from "./BlockQuoteEditorElement";

export const BlockQuotePlugin: EditorElementPlugin = {
    toolbarButton: BlockQuoteButton,
    editorElementRenderer: BlockQuoteEditorElement,
    type: "block-quote",
};
