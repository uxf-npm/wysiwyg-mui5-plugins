import FormatItalicIcon from "@mui/icons-material/FormatItalic";
import React, { FC } from "react";
import { ToolbarButtonProps } from "@uxf/wysiwyg";
import { LeafTypeButton } from "../LeafTypeButton";

export const ItalicButton: FC<ToolbarButtonProps> = ({ editor }) => (
    <LeafTypeButton editor={editor} label={<FormatItalicIcon />} name="italic" title="Kurzíva" />
);
