import React, { FC } from "react";
import { EditorElementRendererProps } from "@uxf/wysiwyg";

export const BlockQuoteEditorElement: FC<EditorElementRendererProps> = ({ attributes, children }) => (
    <blockquote {...attributes}>{children}</blockquote>
);
