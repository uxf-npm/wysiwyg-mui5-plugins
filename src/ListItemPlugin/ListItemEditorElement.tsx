import React, { FC } from "react";
import { EditorElementRendererProps } from "@uxf/wysiwyg";

export const ListItemEditorElement: FC<EditorElementRendererProps> = ({ attributes, children }) => {
    return <li {...attributes}>{children}</li>;
};
