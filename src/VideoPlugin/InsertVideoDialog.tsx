import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import TextField from "@mui/material/TextField";
import { EditorVideoElement, UXFEditor } from "@uxf/wysiwyg";
import React, { FC, useCallback, useMemo, useState } from "react";
import ReactPlayer from "react-player/lazy";

export interface InsertVideoDialogProps {
    open: boolean;
    onClose: () => void;
    editor: UXFEditor;
    activeVideo: EditorVideoElement | null;
}

export const InsertVideoDialog: FC<InsertVideoDialogProps> = ({ open, onClose, editor, activeVideo }) => {
    const [url, setUrl] = useState<string>(activeVideo?.url ?? "");

    const resetState = useCallback(() => {
        setUrl("");
    }, []);

    // Cancel button
    const closeHandler = useCallback(() => {
        resetState();
        onClose();
    }, [onClose, resetState]);

    // OK button
    const okButtonHandler = () => {
        editor.insertVideo(editor, url, editor.selection);
        resetState();
        onClose();
    };

    const playableVideo = useMemo(() => url && ReactPlayer.canPlay(url), [url]);

    return (
        <Dialog open={open} onClose={closeHandler} aria-labelledby="insert-video-dialog" fullWidth>
            <DialogTitle id="insert-video-dialog-title">Vložit video</DialogTitle>
            <DialogContent>
                <TextField
                    margin="dense"
                    size="small"
                    id="url"
                    label="URL videa"
                    type="text"
                    fullWidth
                    value={url}
                    required
                    onChange={e => setUrl(e.target.value)}
                />
                {playableVideo && <ReactPlayer url={url} width="100%" controls={true} />}
            </DialogContent>

            <DialogActions>
                <Button onClick={closeHandler}>Zrušit</Button>
                <Button onClick={okButtonHandler} color="primary" variant="contained" disabled={!playableVideo}>
                    Vložit video
                </Button>
            </DialogActions>
        </Dialog>
    );
};
