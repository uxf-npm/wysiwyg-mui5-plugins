import VideoIcon from "@mui/icons-material/OndemandVideo";
import ToggleButton from "@mui/material/ToggleButton";
import React, { FC, useCallback, useState } from "react";
import { EditorVideoElement, ToolbarButtonProps } from "@uxf/wysiwyg";
import { InsertVideoDialog } from "./InsertVideoDialog";

export const VideoElementButton: FC<ToolbarButtonProps> = ({ editor }) => {
    const [dialogOpen, setDialogOpen] = useState<boolean>(false);
    const [activeVideo, setActiveVideo] = useState<EditorVideoElement | null>(null);

    const closeDialog = useCallback(() => setDialogOpen(false), []);

    return (
        <>
            <ToggleButton
                key="video-button"
                size="small"
                value="video"
                aria-label="video"
                selected={editor.isVideoActive(editor)}
                title="Video"
                onClick={e => {
                    e.preventDefault();
                    e.persist();
                    setActiveVideo(editor.getActiveVideo(editor));
                    setDialogOpen(true);
                }}
            >
                <VideoIcon />
            </ToggleButton>
            <InsertVideoDialog open={dialogOpen} onClose={closeDialog} editor={editor} activeVideo={activeVideo} />
        </>
    );
};
