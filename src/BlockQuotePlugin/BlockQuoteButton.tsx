import FormatQuoteIcon from "@mui/icons-material/FormatQuote";
import { ToolbarButtonProps } from "@uxf/wysiwyg";
import React, { FC } from "react";
import { ElementTypeButton } from "../ElementTypeButton";

export const BlockQuoteButton: FC<ToolbarButtonProps> = ({ editor }) => (
    <ElementTypeButton editor={editor} label={<FormatQuoteIcon />} name="block-quote" title="Citace" />
);
