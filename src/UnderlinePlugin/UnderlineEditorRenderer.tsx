import React, { FC } from "react";
import { EditorLeafRendererProps } from "@uxf/wysiwyg";

export const UnderlineEditorRenderer: FC<EditorLeafRendererProps> = ({ children, leaf }) => {
    if (leaf.underline) {
        return <u>{children}</u>;
    }

    return children;
};
