import { EditorLeafPlugin } from "@uxf/wysiwyg";
import { UnderlineButton } from "./UnderlineButton";
import { UnderlineEditorRenderer } from "./UnderlineEditorRenderer";

export const UnderlinePlugin: EditorLeafPlugin = {
    toolbarButton: UnderlineButton,
    editorLeafRenderer: UnderlineEditorRenderer,
    type: "underline",
};
