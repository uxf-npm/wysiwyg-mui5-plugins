import { ReactElement } from "react";
import { LeafMarks, ToolbarButtonProps } from "@uxf/wysiwyg";

export interface LeafTypeButtonProps extends ToolbarButtonProps {
    name: LeafMarks;
    title: string;
    label: ReactElement;
}
