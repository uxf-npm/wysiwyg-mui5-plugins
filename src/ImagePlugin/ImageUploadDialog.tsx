import { Box } from "@mui/material";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import { EditorImageElement, ImageUploadHandler, UXFEditor } from "@uxf/wysiwyg";
import React, { FC, Reducer, useCallback, useEffect, useReducer, useRef } from "react";
import { Transforms } from "slate";
import { jsx } from "slate-hyperscript";

export interface ImageUploadDialogProps {
    open: boolean;
    onClose: () => void;
    imageUploadHandler: ImageUploadHandler;
    imageNamespace: string;
    editor: UXFEditor;
    editedImage: EditorImageElement | null;
}

const emptyImage: EditorImageElement = {
    type: "image",
    alt: "",
    caption: "",
    source: "",
    children: [jsx("text")],
};

interface State {
    image: EditorImageElement;
    preview: File | null;
}

const initialState: State = {
    image: emptyImage,
    preview: null,
};

type Action =
    | { type: "reset" }
    | ({ type: "setImage" } & Pick<State, "image">)
    | { type: "setCaption"; text: string }
    | { type: "setAlt"; text: string }
    | { type: "setSource"; text: string }
    | ({ type: "setPreview" } & Pick<State, "preview">);

const reducer: Reducer<State, Action> = (prevState, action) => {
    switch (action.type) {
        case "reset":
            return initialState;
        case "setImage":
            return { ...prevState, image: action.image };
        case "setPreview":
            return { ...prevState, preview: action.preview };
        case "setAlt":
            return {
                ...prevState,
                image: {
                    ...prevState.image,
                    alt: action.text,
                },
            };
        case "setCaption":
            return {
                ...prevState,
                image: {
                    ...prevState.image,
                    caption: action.text,
                },
            };
        case "setSource":
            return {
                ...prevState,
                image: {
                    ...prevState.image,
                    source: action.text,
                },
            };
        default:
            throw Error();
    }
};

export const ImageUploadDialog: FC<ImageUploadDialogProps> = ({
    open,
    onClose,
    imageUploadHandler,
    imageNamespace,
    editor,
    editedImage,
}) => {
    const [state, dispatch] = useReducer(reducer, { ...initialState });
    useEffect(() => {
        // this prevents losing reference to edited image
        if (editedImage) {
            dispatch({ type: "setImage", image: editedImage });
        }
    }, [editedImage]);
    const uploadInputRef = useRef<HTMLInputElement>(null);

    const uploadHandler = useCallback(
        async (e: React.ChangeEvent<HTMLInputElement>): Promise<void> => {
            e.preventDefault();
            e.persist();
            if (e.target.files) {
                const uploadedImage = await imageUploadHandler(e.target.files[0], imageNamespace);
                dispatch({ type: "setPreview", preview: e.target.files[0] });
                const image: EditorImageElement = {
                    ...state.image,
                    name: uploadedImage.name,
                    extension: uploadedImage.extension,
                    uuid: uploadedImage.uuid,
                    namespace: uploadedImage.namespace as string,
                    url: uploadedImage.url,
                };
                dispatch({ type: "setImage", image });
            }
        },
        [imageNamespace, imageUploadHandler, state.image],
    );

    // Cancel button
    const closeHandler = useCallback(() => {
        dispatch({ type: "reset" });
        onClose();
    }, [onClose]);

    // OK button
    const insertImageHandler = () => {
        if (!editedImage) {
            editor.insertImage(editor, state.image, editor.selection);
        }

        if (editedImage) {
            Transforms.setNodes(
                editor,
                {
                    caption: !state.image.caption ? undefined : state.image.caption,
                    source: !state.image.source ? undefined : state.image.source,
                    alt: !state.image.alt ? undefined : state.image.alt,
                },
                { at: editor.selection ?? undefined },
            );
        }

        closeHandler();
    };

    return (
        <Dialog open={open} onClose={closeHandler} aria-labelledby="insert-image-dialog" fullWidth>
            <DialogTitle>{editedImage ? "Upravit obrázek" : "Nahrát obrázek"}</DialogTitle>
            <DialogContent>
                <TextField
                    /* eslint-disable-next-line jsx-a11y/no-autofocus */
                    autoFocus
                    margin="dense"
                    size="small"
                    label="Popisek obrázku"
                    type="text"
                    fullWidth
                    value={state.image.caption}
                    onChange={e => {
                        e.persist();
                        dispatch({ type: "setCaption", text: e.target.value });
                    }}
                />
                <TextField
                    margin="dense"
                    size="small"
                    label="Zdroj obrázku"
                    type="text"
                    fullWidth
                    value={state.image.source}
                    onChange={e => {
                        e.persist();
                        dispatch({ type: "setSource", text: e.target.value });
                    }}
                />
                <TextField
                    margin="dense"
                    size="small"
                    label="Alt atribut"
                    type="text"
                    fullWidth
                    value={state.image.alt}
                    onChange={e => {
                        e.persist();
                        dispatch({ type: "setAlt", text: e.target.value });
                    }}
                />

                {state.preview && !editedImage && (
                    <Box mt={2}>
                        <img
                            src={URL.createObjectURL(state.preview)}
                            alt={state.image.alt ?? ""}
                            height={250}
                            width={555}
                            style={{ width: "100%", height: 250, objectFit: "scale-down", objectPosition: "center" }}
                        />
                        <div>
                            <Typography variant="caption">* Obrázek je zmenšen pro účely náhledu</Typography>
                        </div>
                    </Box>
                )}

                {!editedImage && (
                    <Box mt={2} justifyContent="center" display="flex">
                        <input ref={uploadInputRef} type="file" onChange={uploadHandler} accept="image/*" hidden />
                        <Button size="medium" variant="outlined" onClick={() => uploadInputRef.current?.click()}>
                            Vybrat soubor
                        </Button>
                    </Box>
                )}
            </DialogContent>

            <DialogActions>
                <Button onClick={closeHandler}>Zrušit</Button>
                <Button onClick={insertImageHandler} color="primary" variant="contained" disabled={!state.image.uuid}>
                    {editedImage ? "Upravit obrázek" : "Vložit obrázek"}
                </Button>
            </DialogActions>
        </Dialog>
    );
};
