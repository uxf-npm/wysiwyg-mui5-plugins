import { Box } from "@mui/material";
import React, { FC } from "react";
import { useFocused, useSelected } from "slate-react";
import { EditorImageElementRendererProps } from "@uxf/wysiwyg";
import { Image } from "./Image";

export const ImageEditorElement: FC<EditorImageElementRendererProps> = ({
    attributes,
    element,
    imageUrlClient,
    imageNamespace,
    children,
}) => {
    const selected = useSelected();
    const focused = useFocused();

    return (
        <Box {...attributes} mb={2}>
            <figure contentEditable={false}>
                <Image
                    focused={focused}
                    selected={selected}
                    src={imageUrlClient(element.uuid, element.extension, imageNamespace)}
                    alt={element.alt}
                />
                {(element.caption || element.source) && (
                    <figcaption>
                        {element.caption}
                        {element.source && (
                            <>
                                {element.caption && <br />}
                                <cite>{element.source}</cite>
                            </>
                        )}
                    </figcaption>
                )}
            </figure>
            {children}
        </Box>
    );
};
