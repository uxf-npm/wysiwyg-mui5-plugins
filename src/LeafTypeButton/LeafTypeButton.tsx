import ToggleButton from "@mui/material/ToggleButton";
import React, { FC } from "react";
import { LeafTypeButtonProps } from "./types";

export const LeafTypeButton: FC<LeafTypeButtonProps> = ({ editor, label, name, title }) => {
    return (
        <ToggleButton
            key={name}
            size="small"
            title={title}
            value={name}
            aria-label={name}
            selected={editor.isMarkActive(editor, name)}
            onMouseDown={event => {
                editor.toggleMark(editor, name);
                event.preventDefault();
            }}
        >
            {label}
        </ToggleButton>
    );
};
