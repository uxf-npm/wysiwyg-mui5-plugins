import { Theme } from "@mui/material";

export const createEditableStyles = (theme: Theme) => ({
    border: `1px solid ${theme.palette.divider}`,
    borderTop: "none",
    borderRadius: theme.spacing(0, 0, 0.5, 0.5),
    marginTop: -1,
    padding: theme.spacing(2),
    minHeight: theme.spacing(8),
});
