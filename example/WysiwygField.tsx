import { useTheme } from "@mui/material";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import { EditorImageElementPlugin, WysiwygContent, WysiwygEditor } from "@uxf/wysiwyg";
import { ContentRenderer, RenderComponent } from "@uxf/wysiwyg/Content";
import React, { FC } from "react";
import { useField } from "react-final-form";

import {
    BlockQuotePlugin,
    BoldPlugin,
    ButtonPlugin,
    CodePlugin,
    createEditableStyles,
    Divider,
    HeadingPlugins,
    ImagePlugin,
    ItalicPlugin,
    LinkPlugin,
    ListBulletedPlugin,
    ListItemPlugin,
    ListNumberedPlugin,
    ParagraphPlugin,
    Toolbar,
    UnderlinePlugin,
    VideoPlugin,
} from "../src";
import { ButtonElement, ParagraphComponent } from "./components";

// image url mock
const imageUrlClient = () => "https://cdn.pixabay.com/photo/2020/05/20/06/47/mountain-5195052_960_720.jpg";

const imageUploadHandler = () => {
    return Promise.resolve({
        id: 1,
        uuid: "uuid",
        type: "image",
        extension: ".jpg",
        name: "name",
        namespace: "blog",
        url: "https://cdn.pixabay.com/photo/2020/05/20/06/47/mountain-5195052_960_720.jpg",
    });
};

export const WysiwygField: FC = () => {
    const { input } = useField<WysiwygContent>("wysiwyg");
    const theme = useTheme();
    const MyImagePlugin: EditorImageElementPlugin = {
        ...ImagePlugin,
        imageUrlClient: imageUrlClient,
        imageUploadHandler: imageUploadHandler,
        imageNamespace: "blog",
    };

    return (
        <>
            <Box mb={2}>
                <WysiwygEditor
                    onChange={input.onChange}
                    value={input.value}
                    Toolbar={Toolbar}
                    plugins={[
                        ParagraphPlugin,
                        ...HeadingPlugins,
                        Divider,
                        ItalicPlugin,
                        BoldPlugin,
                        UnderlinePlugin,
                        CodePlugin,
                        BlockQuotePlugin,
                        Divider,
                        LinkPlugin,
                        ButtonPlugin,
                        Divider,
                        ListNumberedPlugin,
                        ListBulletedPlugin,
                        ListItemPlugin,
                        Divider,
                        VideoPlugin,
                        MyImagePlugin,
                    ]}
                    stylesForEditable={createEditableStyles(theme)}
                />
            </Box>
            <Box
                sx={{
                    minHeight: 200,
                }}
            >
                <Card variant="outlined">
                    <CardContent>
                        <ContentRenderer<{ prop: string }, { "solar-lead": RenderComponent }>
                            data={input.value}
                            components={{
                                buttonComponent: ButtonElement,
                                customRenderers: {
                                    "solar-lead": ParagraphComponent,
                                },
                            }}
                            defaultComponentProps={{
                                button: {
                                    ownProps: {
                                        prop: "This is custom prop passed to button component",
                                    },
                                },
                            }}
                        />
                    </CardContent>
                </Card>
            </Box>
        </>
    );
};

WysiwygField.displayName = "WysiwygField";
