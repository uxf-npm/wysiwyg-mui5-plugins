import { EditorElementPlugin } from "@uxf/wysiwyg";
import { ListBulletedButton } from "./ListBulletedButton";
import { ListBulletedEditorElement } from "./ListBulletedEditorElement";

export const ListBulletedPlugin: EditorElementPlugin = {
    toolbarButton: ListBulletedButton,
    editorElementRenderer: ListBulletedEditorElement,
    type: "bulleted-list",
};
