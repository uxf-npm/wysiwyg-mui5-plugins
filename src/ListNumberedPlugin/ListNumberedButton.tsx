import FormatListNumberedIcon from "@mui/icons-material/FormatListNumbered";
import React, { FC } from "react";
import { ToolbarButtonProps } from "@uxf/wysiwyg";
import { ElementTypeButton } from "../ElementTypeButton";

export const ListNumberedButton: FC<ToolbarButtonProps> = ({ editor }) => (
    <ElementTypeButton
        editor={editor}
        label={<FormatListNumberedIcon />}
        name="numbered-list"
        title="Číslovaný seznam"
    />
);
