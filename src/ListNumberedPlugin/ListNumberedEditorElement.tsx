import React, { FC } from "react";
import { EditorElementRendererProps } from "@uxf/wysiwyg";

export const ListNumberedEditorElement: FC<EditorElementRendererProps> = ({ attributes, children }) => {
    return <ol {...attributes}>{children}</ol>;
};
