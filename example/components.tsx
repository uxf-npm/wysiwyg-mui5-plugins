import React, { FC } from "react";
import {
    RenderButtonComponentProps,
    RenderComponentProps,
    RenderImageComponentProps,
    RenderVideoComponentProps,
} from "@uxf/wysiwyg/Content";
import { Box, Button } from "@mui/material";

export const ParagraphComponent: FC<RenderComponentProps> = ({ children }) => {
    return <p>{children}</p>;
};

export const ImageComponent: FC<RenderImageComponentProps> = ({ image }) => {
    return (
        <div style={{ marginBottom: "2em", width: "100%" }} id="custom-image-component">
            <figure contentEditable={false}>
                <img src={image.url} alt={image.caption} style={{ width: "100%" }} />
                {(image.caption || image.source) && (
                    <figcaption>
                        {image.caption}
                        {image.source && (
                            <>
                                {image.caption && <br />}
                                <cite>{image.source}</cite>
                            </>
                        )}
                    </figcaption>
                )}
            </figure>
        </div>
    );
};

export interface ButtonExampleProps {
    prop: string;
}

export const ButtonElement: FC<RenderButtonComponentProps<ButtonExampleProps | undefined>> = ({ button, ownProps }) => {
    // eslint-disable-next-line no-console
    console.log(ownProps);
    return (
        <Box display="flex" justifyContent="center" mb={2}>
            <Button href={button.buttonUrl} variant="outlined">
                {button.buttonText}
            </Button>
        </Box>
    );
};
export const VideoComponent: FC<RenderVideoComponentProps> = ({ children }) => {
    return <div>{children}</div>;
};
