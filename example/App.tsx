import React from "react";
import { Form } from "react-final-form";
import { WysiwygField } from "./WysiwygField";

const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

const onSubmit = async (values: any) => {
    await sleep(300);
    // eslint-disable-next-line no-alert
    window.alert(JSON.stringify(values));
};

export const App: React.FC = () => {
    return (
        <Form
            onSubmit={onSubmit}
            initialValues={{
                wysiwyg: [
                    {
                        type: "paragraph",
                        children: [{ text: "" }],
                    },
                ],
            }}
        >
            {({ handleSubmit }) => {
                return (
                    <form onSubmit={handleSubmit}>
                        <div>
                            <WysiwygField />
                        </div>
                    </form>
                );
            }}
        </Form>
    );
};
