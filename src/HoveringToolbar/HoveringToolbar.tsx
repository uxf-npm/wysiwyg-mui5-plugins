import Paper from "@mui/material/Paper";
import { ToolbarComponent } from "@uxf/wysiwyg";
import React, { createRef, useEffect, useState } from "react";
import { createPortal } from "react-dom";
import { Editor, Range } from "slate";
import { ReactEditor, useSlate } from "slate-react";

export const HoveringToolbar: ToolbarComponent = ({ plugins }) => {
    const ref = createRef<any>();
    const editor = useSlate();
    const { selection } = editor;
    const w = typeof window !== "undefined" ? window : null;
    const domSelection = w?.getSelection();
    const [position, setPosition] = useState<{ top: number; left: number } | null>(null);

    useEffect(() => {
        let domRange = null;
        let rect = null;

        if (domSelection && domSelection.rangeCount > 0) {
            domRange = domSelection.getRangeAt(0);
            rect = domRange.getBoundingClientRect();
        }

        if (ref.current && rect && w) {
            setPosition({
                top: rect.top + w.pageYOffset - ref.current.offsetHeight,
                left: rect.left + w.pageXOffset - ref.current.offsetWidth / 2 + rect.width / 2,
            });
        }
    }, [selection, domSelection, w, ref]);

    if (
        !selection ||
        !ReactEditor.isFocused(editor) ||
        Range.isCollapsed(selection) ||
        Editor.string(editor, selection) === ""
    ) {
        return null;
    }

    return (
        <>
            {createPortal(
                <Paper
                    elevation={0}
                    ref={ref}
                    style={{
                        display: "inline-block",
                        position: "absolute",
                        margin: 0,
                        marginTop: -6,
                        top: position ? position.top : -1000,
                        left: position ? position.left : -1000,
                    }}
                >
                    {plugins.map((Plugin, index) => {
                        if ("buttonDivider" in Plugin) {
                            return <Plugin.buttonDivider key={`divider-${index}`} />;
                        }

                        if (typeof Plugin.toolbarButton === "undefined") {
                            return null;
                        }

                        if ("imageUploadHandler" in Plugin && "imageNamespace" in Plugin) {
                            return (
                                <Plugin.toolbarButton
                                    key={Plugin.type + index}
                                    editor={editor}
                                    imageNamespace={Plugin.imageNamespace}
                                    imageUploadHandler={Plugin.imageUploadHandler}
                                />
                            );
                        }

                        return <Plugin.toolbarButton key={Plugin.type + index} editor={editor} />;
                    })}
                </Paper>,
                document.body,
            )}
        </>
    );
};
