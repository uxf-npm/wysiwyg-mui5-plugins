import React, { FC } from "react";
import { EditorElementRendererProps } from "@uxf/wysiwyg";

export const ListBulletedEditorElement: FC<EditorElementRendererProps> = ({ attributes, children }) => {
    return <ul {...attributes}>{children}</ul>;
};
