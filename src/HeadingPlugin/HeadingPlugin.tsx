import { EditorElementPlugin } from "@uxf/wysiwyg";
import { H1Button, H2Button, H3Button, H4Button, H5Button, H6Button } from "./HeadingButtons";
import { HeadingEditorElement } from "./HeadingEditorElement";

export const H1Plugin: EditorElementPlugin = {
    toolbarButton: H1Button,
    editorElementRenderer: HeadingEditorElement,
    type: "h1",
};

export const H2Plugin: EditorElementPlugin = {
    toolbarButton: H2Button,
    editorElementRenderer: HeadingEditorElement,
    type: "h2",
};

export const H3Plugin: EditorElementPlugin = {
    toolbarButton: H3Button,
    editorElementRenderer: HeadingEditorElement,
    type: "h3",
};

export const H4Plugin: EditorElementPlugin = {
    toolbarButton: H4Button,
    editorElementRenderer: HeadingEditorElement,
    type: "h4",
};

export const H5Plugin: EditorElementPlugin = {
    toolbarButton: H5Button,
    editorElementRenderer: HeadingEditorElement,
    type: "h5",
};

export const H6Plugin: EditorElementPlugin = {
    toolbarButton: H6Button,
    editorElementRenderer: HeadingEditorElement,
    type: "h",
};
