import CodeIcon from "@mui/icons-material/Code";
import React, { FC } from "react";
import { ToolbarButtonProps } from "@uxf/wysiwyg";
import { LeafTypeButton } from "../LeafTypeButton";

export const CodeButton: FC<ToolbarButtonProps> = ({ editor }) => (
    <LeafTypeButton editor={editor} label={<CodeIcon />} name="code" title="Zdrojový kód" />
);
