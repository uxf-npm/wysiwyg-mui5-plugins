import ToggleButton from "@mui/material/ToggleButton";
import React, { FC } from "react";
import { ElementTypeButtonProps } from "./types";

export const ElementTypeButton: FC<ElementTypeButtonProps> = ({ editor, label, name, title }) => {
    return (
        <ToggleButton
            key={name}
            size="small"
            title={title}
            value={name}
            aria-label={name}
            selected={editor.isElementActive(editor, name)}
            onMouseDown={event => {
                editor.toggleElement(editor, name);
                event.preventDefault();
            }}
        >
            {label}
        </ToggleButton>
    );
};
