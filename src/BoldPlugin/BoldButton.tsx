import FormatBoldIcon from "@mui/icons-material/FormatBold";
import React, { FC } from "react";
import { ToolbarButtonProps } from "@uxf/wysiwyg";
import { LeafTypeButton } from "../LeafTypeButton";

export const BoldButton: FC<ToolbarButtonProps> = ({ editor }) => (
    <LeafTypeButton editor={editor} label={<FormatBoldIcon />} name="bold" title="Tučné" />
);
